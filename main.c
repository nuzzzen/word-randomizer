#include <string.h>
#include <stdio.h>
#include <stdlib.h>

void stringMix(char str[]){
    int len = strlen(str);
    for (int i=0; i<len; i++) {
        int random_index = random()%len;
        str[i] = str[i]+str[random_index]-(str[random_index]=str[i]);
    }
    printf("\n%s\n",str);
}

int main(){
    char str[] = {"Ciao, se conosci persone interessate e disponibili a lavorare in Auditorium sin da subito, compresi i giorni festivi, puoi farci inviare un curriculum vitae con allegata foto a [redacted]@[redacted].it oppure girarlo direttamente qui su whatsapp. Grazie"};
    printf("\n\nString %s\n\n\n", str);

    stringMix(str);
    return 0;
}